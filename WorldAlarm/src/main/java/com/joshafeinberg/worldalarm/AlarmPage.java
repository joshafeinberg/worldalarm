package com.joshafeinberg.worldalarm;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


public class AlarmPage extends ActionBarActivity implements FragmentManager.OnBackStackChangedListener {

    /**
     * {@inheritDoc}
     * @param savedInstanceState {@link android.os.Bundle}
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_page);

        if (findViewById(R.id.container) != null) {

            if (savedInstanceState != null) {
                return;
            }

            AlarmList alarmListFragment = new AlarmList();
            alarmListFragment.setArguments(getIntent().getExtras());
            getSupportFragmentManager().beginTransaction().add(R.id.container, alarmListFragment).commit();

        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onResume() {
        super.onResume();
        getSupportFragmentManager().addOnBackStackChangedListener(this);
    }

    /**
     * {@inheritDoc}
     *
     * @param menu {@link android.view.Menu}
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.alarm_page, menu);
        int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
        if(backStackEntryCount > 0){
            menu.findItem(R.id.action_add).setVisible(false);
        } else {
            menu.findItem(R.id.action_add).setVisible(true);
        }
        return true;
    }


    /**
     * {@inheritDoc}
     *
     * @param item The menu item that was selected.
     * @return Return false to allow normal menu processing to proceed, true to consume it here.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getSupportFragmentManager().popBackStackImmediate();
                return true;
            case R.id.action_add:
                addNewAlarm();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }


    /**
     * Pushes the {@link com.joshafeinberg.worldalarm.AddAlarm} fragment onto the screen
     */
    public void addNewAlarm() {
        AddAlarm addAlarmFragment = new AddAlarm();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, addAlarmFragment);
        transaction.addToBackStack(null);

        transaction.commit();
    }




    /**
     * {@inheritDoc}
     *
     * Additionally, determines if back button should be display
     */
    @Override
    public void onBackStackChanged() {
        ActionBar actionBar = getSupportActionBar();
        int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
        if(backStackEntryCount > 0){
            actionBar.setDisplayHomeAsUpEnabled(true);
        }else{
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
        supportInvalidateOptionsMenu();
    }


}

package com.joshafeinberg.worldalarm.Database;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class AlarmTable {

    public static final String TABLE_NAME = "alarms";
    public static final String COLUMN_ID = "AlarmID";
    public static final String COLUMN_NAME = "AlarmName";
    public static final String COLUMN_UTCTIME = "UTCTime";
    public static final String COLUMN_TIMEZONE = "TimeZone";
    public static final String COLUMN_LOCATION = "Location";
    public static final String COLUMN_RECURTYPE = "RecurType";
    public static final String COLUMN_ACTIVE = "Active";

    private static final String DATABASE_CREATE =
            "create table " + TABLE_NAME + "("
                    + COLUMN_ID + " integer primary key autoincrement, "
                    + COLUMN_NAME + " text, "
                    + COLUMN_UTCTIME + " integer not null, "
                    + COLUMN_TIMEZONE + " text, "
                    + COLUMN_LOCATION + " text, "
                    + COLUMN_RECURTYPE + " integer, "
                    + COLUMN_ACTIVE + " integer);";

    /**
     * creates table in database using SQLite
     * @param database database to hold alarms
     */
    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    /**
     * drops and recreates table on upgrades
     * @param database database to hold alarms
     * @param oldVersion id of old database
     * @param newVersion id of new database
     */
    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(AlarmTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(database);
    }
}
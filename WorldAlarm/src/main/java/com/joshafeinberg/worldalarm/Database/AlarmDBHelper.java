package com.joshafeinberg.worldalarm.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.joshafeinberg.worldalarm.AddAlarm;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

public class AlarmDBHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "alarms.db";
    private static final int DATABASE_VERSION = 2;
    private static Context context;

    /**
     * @param context activity context
     * @see android.database.sqlite.SQLiteOpenHelper
     */
    public AlarmDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        AlarmDBHelper.context = context;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate(SQLiteDatabase database) {
        AlarmTable.onCreate(database);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion,
                          int newVersion) {
        AlarmTable.onUpgrade(database, oldVersion, newVersion);
    }


    /**
     * Adds new alarm to database
     * @param alarm new alarm
     * @see com.joshafeinberg.worldalarm.Database.Alarm
     */
    public long addAlarm(Alarm alarm) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(AlarmTable.COLUMN_NAME, alarm.getAlarmName());
        values.put(AlarmTable.COLUMN_UTCTIME, alarm.getUTCTime());
        values.put(AlarmTable.COLUMN_TIMEZONE, alarm.getTimeZone());
        values.put(AlarmTable.COLUMN_LOCATION, alarm.getLocation());
        values.put(AlarmTable.COLUMN_RECURTYPE, alarm.getRecurType());
        values.put(AlarmTable.COLUMN_ACTIVE, alarm.getActive());

        long row = -1;
        // Inserting Row
        if (db != null) {
            row = db.insert(AlarmTable.TABLE_NAME, null, values);
            db.close();
        }

        return row;
    }

    /**
     * Retrieves an individual alarm from database
     * @param AlarmID database id of alarm
     * @return alarm {@link com.joshafeinberg.worldalarm.Database.Alarm}
     */
    public Alarm getAlarm(int AlarmID) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = null;
        if (db != null) {
            cursor = db.query(AlarmTable.TABLE_NAME,
                    new String[] { AlarmTable.COLUMN_ID, AlarmTable.COLUMN_NAME, AlarmTable.COLUMN_UTCTIME,
                            AlarmTable.COLUMN_TIMEZONE, AlarmTable.COLUMN_LOCATION, AlarmTable.COLUMN_RECURTYPE,
                            AlarmTable.COLUMN_ACTIVE},
                    AlarmTable.COLUMN_ID + "=?",
                    new String[] { String.valueOf(AlarmID) }, null, null, null, null);
        }

        if (cursor != null) {
            cursor.moveToFirst();
        }

        Alarm alarm = null;
        if (cursor != null && cursor.getCount() > 0) {
            alarm = new Alarm(cursor.getInt(0), cursor.getString(1), cursor.getLong(2),
                    cursor.getString(3), cursor.getString(4), cursor.getInt(5), cursor.getInt(6));
        }

        return alarm;
    }

    /**
     * Retrieves all alarm from database
     * @return {@link java.util.List} containing all alarms
     */
    public List<Alarm> getAllAlarms() {
        List<Alarm> alarmList = new ArrayList<Alarm>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + AlarmTable.TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor;
        if (db != null) {
            cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    Alarm alarm = new Alarm(cursor.getInt(0), cursor.getString(1), cursor.getLong(2),
                            cursor.getString(3), cursor.getString(4), cursor.getInt(5), cursor.getInt(6));
                    alarmList.add(alarm);
                } while (cursor.moveToNext());
            }

        }

        return alarmList;
    }

    public Alarm getNextAlarm() {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = null;
        if (db != null) {
            cursor = db.query(AlarmTable.TABLE_NAME,
                    new String[] { AlarmTable.COLUMN_ID, AlarmTable.COLUMN_NAME, AlarmTable.COLUMN_UTCTIME,
                            AlarmTable.COLUMN_TIMEZONE, AlarmTable.COLUMN_LOCATION, AlarmTable.COLUMN_RECURTYPE,
                            AlarmTable.COLUMN_ACTIVE},
                    AlarmTable.COLUMN_UTCTIME + ">?",
                    new String[] { Long.toString(new Date().getTime()) }, null, null, AlarmTable.COLUMN_UTCTIME + " ASC", "1");
        }

        if (cursor != null) {
            cursor.moveToFirst();
        }

        Alarm alarm = null;
        if (cursor != null && cursor.getCount() > 0) {
            alarm = new Alarm(cursor.getInt(0), cursor.getString(1), cursor.getLong(2),
                    cursor.getString(3), cursor.getString(4), cursor.getInt(5), cursor.getInt(6));
        }

        return alarm;
    }


    /**
     * updates an alarm based on the alarm id
     * @param alarm alarm with new data
     * @return success or failure
     */
    public int updateAlarm(Alarm alarm) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(AlarmTable.COLUMN_NAME, alarm.getAlarmName());
        values.put(AlarmTable.COLUMN_UTCTIME, alarm.getUTCTime());
        values.put(AlarmTable.COLUMN_TIMEZONE, alarm.getTimeZone());
        values.put(AlarmTable.COLUMN_LOCATION, alarm.getLocation());
        values.put(AlarmTable.COLUMN_RECURTYPE, alarm.getRecurType());
        values.put(AlarmTable.COLUMN_ACTIVE, alarm.getActive());



        if (db != null) {
            return db.update(AlarmTable.TABLE_NAME, values, AlarmTable.COLUMN_ID + " = ?",
                    new String[] { String.valueOf(alarm.getAlarmID()) });
        }

        return 0;
    }


    /**
     * deletes an alarm based on alarm id
     * @param alarmID the alarms database id
     */
    public void deleteAlarm(int alarmID) {
        AddAlarm.removeAlarmFromManager(context, alarmID);

        SQLiteDatabase db = this.getWritableDatabase();
        if (db != null) {
            db.delete(AlarmTable.TABLE_NAME, AlarmTable.COLUMN_ID + " = ?",
                    new String[] { Integer.toString(alarmID) });
            db.close();
        }
    }

    /**
     * switches if an alarm is active or not
     * @param alarmID the alarms database id
     * @param active  flag to make active or deactivated
     * @return success flag
     */
    public int setAlarmActive(int alarmID, boolean active) {
        SQLiteDatabase db = this.getWritableDatabase();

        Alarm alarm = getAlarm(alarmID);
        if (alarm != null) {
            ContentValues values = new ContentValues();
            values.put(AlarmTable.COLUMN_NAME, alarm.getAlarmName());
            values.put(AlarmTable.COLUMN_UTCTIME, alarm.getUTCTime());
            values.put(AlarmTable.COLUMN_TIMEZONE, alarm.getTimeZone());
            values.put(AlarmTable.COLUMN_LOCATION, alarm.getLocation());
            values.put(AlarmTable.COLUMN_RECURTYPE, alarm.getRecurType());
            if (active) {
                values.put(AlarmTable.COLUMN_ACTIVE, Alarm.ACTIVE);
            } else {
                values.put(AlarmTable.COLUMN_ACTIVE, Alarm.INACTIVE);
            }

            //TODO: if alarm.getUTCTime() is before current time, reset it for next time possible
            // possibly just run Alarm.updateRecurringAlarm


            if (db != null) {
                return db.update(AlarmTable.TABLE_NAME, values, AlarmTable.COLUMN_ID + " = ?",
                        new String[] { String.valueOf(alarm.getAlarmID()) });
            }
        }

        return 0;
    }
}

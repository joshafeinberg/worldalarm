package com.joshafeinberg.worldalarm.Database;

public class Alarm {

    /**
     * database id
     */
    public int AlarmID;
    /**
     * display name
     */
    public String AlarmName;
    /**
     * alarm time UTC in milliseconds
     */
    public long UTCTime;
    /**
     * timezone id {@link java.util.TimeZone}
     */
    public String TimeZone;
    /**
     * location of timezone
     */
    public String Location;
    /**
     * 0-no repeat; 1-daily; 2-weekdays; 3-weekends; 4-weekly; 5-monthly; 6-yearly
     */
    public int RecurType;
    /**
     * 1 if alarm is active
     */
    public int Active;

    public static final int ACTIVE = 1;
    public static final int INACTIVE = 0;
    public static final String LOCALALARM = "WORLDALARMLOCAL";

    /**
     * Creates a structure to hold all details for
     * an alarm.
     *
     * @param AlarmID {@link #AlarmID}
     * @param AlarmName {@link #AlarmName}
     * @param UTCTime {@link #UTCTime}
     * @param TimeZone {@link #TimeZone}
     * @param Location {@link #Location}
     * @param RecurType {@link #RecurType}
     * @param Active {@link #Active}
     */
    public Alarm(int AlarmID, String AlarmName, long UTCTime, String TimeZone, String Location, int RecurType, int Active) {
        this.AlarmID = AlarmID;
        this.AlarmName = AlarmName;
        this.UTCTime = UTCTime;
        this.TimeZone = TimeZone;
        this.Location = Location;
        this.RecurType = RecurType;
        this.Active = Active;
    }

    /**
     * Creates a structure to hold all details for
     * an alarm.
     *
     * @param AlarmName {@link #AlarmName}
     * @param UTCTime {@link #UTCTime}
     * @param TimeZone {@link #TimeZone}
     * @param Location {@link #Location}
     * @param RecurType {@link #RecurType}
     */
    public Alarm(String AlarmName, long UTCTime, String TimeZone, String Location, int RecurType, int Active) {
        this.AlarmName = AlarmName;
        this.UTCTime = UTCTime;
        this.TimeZone = TimeZone;
        this.Location = Location;
        this.RecurType = RecurType;
        this.Active = Active;
    }

    /**
     * @return {@link #AlarmID}
     */
    public int getAlarmID() {
        return AlarmID;
    }

    /**
     * @return {@link #AlarmName}
     */
    public String getAlarmName() {
        return AlarmName;
    }

    /**
     * @return {@link #UTCTime}
     */
    public long getUTCTime() {
        return UTCTime;
    }

    /**
     * @return {@link #TimeZone}
     */
    public String getTimeZone() {
        return TimeZone;
    }

    /**
     * @return {@link #Location}
     */
    public String getLocation() {
        return Location;
    }

    /**
     * @return {@link #RecurType}
     */
    public int getRecurType() {
        return RecurType;
    }

    /**
     * @return {@link #Active}
     */
    public int getActive() { return Active; }
}

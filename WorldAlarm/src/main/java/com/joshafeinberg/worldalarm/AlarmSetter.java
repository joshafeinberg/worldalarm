package com.joshafeinberg.worldalarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.joshafeinberg.worldalarm.Database.Alarm;
import com.joshafeinberg.worldalarm.Database.AlarmDBHelper;

import java.util.Date;
import java.util.List;
import java.util.TimeZone;


public class AlarmSetter extends BroadcastReceiver {

    /**
     * Called on boot, used to put all alarms back in AlarmManager
     * Also updates alarms to local time
     */
    @Override
    public void onReceive(Context context, Intent intent) {

        AlarmDBHelper alarmDBHelper = new AlarmDBHelper(context);
        List<Alarm> allAlarms = alarmDBHelper.getAllAlarms();

        if (intent.getAction().contains("BOOT_COMPLETED")) {
            for (Alarm alarm : allAlarms) {
                if (alarm.getUTCTime() > new Date().getTime() && alarm.getActive() == 1) {
                    Log.i("worldalarm", "Added Alarm " + alarm.getAlarmID() + ": " + alarm.getAlarmName());
                    AddAlarm.addAlarmToManager(context, alarm.getAlarmID(), alarm.getUTCTime());
                }
            }
        }

        for (Alarm alarm : allAlarms) {
            if (alarm.getLocation().equals(Alarm.LOCALALARM)) {
                if (alarm.getActive() == 1) {
                    AddAlarm.removeAlarmFromManager(context, alarm.getAlarmID());
                }
                long CurrentTime = alarm.getUTCTime();
                int AlarmOffset = TimeZone.getTimeZone(alarm.getTimeZone()).getOffset(CurrentTime);
                int NewOffset = TimeZone.getDefault().getOffset(CurrentTime);
                long HourDifference = (AlarmOffset - NewOffset);
                alarm.UTCTime = alarm.getUTCTime() + HourDifference;
                alarm.TimeZone = TimeZone.getDefault().getID();
                alarmDBHelper.updateAlarm(alarm);
                if (alarm.getActive() == Alarm.ACTIVE) {
                    AddAlarm.addAlarmToManager(context, alarm.getAlarmID(), alarm.getUTCTime());
                }
            }
        }
    }
}

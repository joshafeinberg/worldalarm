package com.joshafeinberg.worldalarm;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.util.Date;

public class AlarmScreen extends Activity {

    private long AlarmID;
    private MediaPlayer mediaPlayer;
    private PowerManager.WakeLock WakeLock;
    NotificationManager nm;

    /**
     * {@inheritDoc}
     *
     * Additionally, sets all the page details and plays the alarm notification
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_alarm_screen);

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        //noinspection deprecation
        WakeLock = pm.newWakeLock((PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), "WorldAlarm");
        WakeLock.acquire();

        Bundle Extras = getIntent().getExtras();
        AlarmID = Extras.getInt("AlarmID");
        Log.i("worldalarm", "Alarm Set Off => " + AlarmID);
        TextView AlarmName = (TextView) findViewById(R.id.AlarmName);
        AlarmName.setText(Extras.getCharSequence("AlarmName"));
        TextView AlarmLocalTime = (TextView) findViewById(R.id.AlarmLocalTime);
        AlarmLocalTime.setText(Extras.getCharSequence("AlarmLocalTime"));
        CharSequence AlarmTimeLocation = Extras.getCharSequence("AlarmTimeLocation");
        if (AlarmTimeLocation == null) {
            AlarmTimeLocation = "";
        }
        if (AlarmTimeLocation.equals("")) {
            TextView AlarmLocation = (TextView) findViewById(R.id.AlarmTimeLocation);
            AlarmLocation.setVisibility(View.GONE);
            TextView InString = (TextView) findViewById(R.id.InString);
            InString.setVisibility(View.GONE);
        } else {
            TextView AlarmLocation = (TextView) findViewById(R.id.AlarmTimeLocation);
            AlarmLocation.setText(Extras.getCharSequence("AlarmLocation"));
        }

        mediaPlayer = new MediaPlayer();
        startAlarm(this, mediaPlayer);

        nm = (NotificationManager)
                getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        Button stopButton = (Button) findViewById(R.id.stopButton);
        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopAlarm(getApplicationContext(), mediaPlayer, AlarmID);
                //mediaPlayer.stop();
                //mediaPlayer.release();
                mediaPlayer = null;
                try {
                    WakeLock.release();
                } catch (Exception e) {};
                //nm.cancel((int) AlarmID);
                //AddAlarm.updateRecurringAlarm(getApplicationContext(), (int)AlarmID);
                finish();
            }
        });

        Button snoozeButton = (Button) findViewById(R.id.snoozeButton);
        snoozeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mediaPlayer.stop();
                //mediaPlayer.release();
                snoozeAlarm(getApplicationContext(), mediaPlayer, AlarmID);
                mediaPlayer = null;
                try {
                    WakeLock.release();
                } catch (Exception e) {};
                //nm.cancel((int)AlarmID);
                //long CurrentTime = new Date().getTime();
                //long SnoozeTime = 7 * 60 * 1000;
                //long NewAlarmTime = CurrentTime + SnoozeTime;
                //AddAlarm.addAlarmToManager(getApplicationContext(), AlarmID, NewAlarmTime);
                finish();

            }
        });
    }

    @Override
    protected void onStop() {
        try {
            super.onStop();
            stopAlarm(getApplicationContext(), mediaPlayer, AlarmID);
            mediaPlayer = null;
            WakeLock.release();
            finish();
        } catch (Exception e) { Log.i("worldalarm", "Already Stopped"); }
    }

    public static void startAlarm(Context context, MediaPlayer mediaPlayer) {
        Uri alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        if(alert == null){
            alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            if(alert == null){
                alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            }
        }

        //mediaPlayer = new MediaPlayer();
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        try {
            mediaPlayer.setDataSource(context, alert);
            if (audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
                mediaPlayer.setLooping(true);
                mediaPlayer.prepare();
                mediaPlayer.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void stopAlarm(Context context, MediaPlayer mediaPlayer, long AlarmID) {
        mediaPlayer.stop();
        mediaPlayer.release();
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.cancel((int) AlarmID);
        AddAlarm.updateRecurringAlarm(context, (int) AlarmID);
    }

    public static void snoozeAlarm(Context context, MediaPlayer mediaPlayer, long AlarmID) {
        mediaPlayer.stop();
        mediaPlayer.release();
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.cancel((int)AlarmID);
        long CurrentTime = new Date().getTime();
        long SnoozeTime = 7 * 60 * 1000;
        long NewAlarmTime = CurrentTime + SnoozeTime;
        AddAlarm.addAlarmToManager(context, AlarmID, NewAlarmTime);
    }
}
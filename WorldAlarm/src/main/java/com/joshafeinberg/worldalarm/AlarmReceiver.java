package com.joshafeinberg.worldalarm;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.PowerManager;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.WindowManager;

import com.joshafeinberg.worldalarm.Database.Alarm;
import com.joshafeinberg.worldalarm.Database.AlarmDBHelper;

import java.util.Date;
import java.util.TimeZone;


public class AlarmReceiver extends BroadcastReceiver {

    /**
     * {@inheritDoc}
     * 
     * Called via the AlarmManager, sets up the {@link com.joshafeinberg.worldalarm.AlarmScreen}
     * and resets alarm depending on {@link com.joshafeinberg.worldalarm.Database.Alarm#RecurType}
     * @param context activity context
     * @param intent  intent
     */
    @Override
    public void onReceive(Context context, Intent intent) {

        long id = intent.getLongExtra("id", 1);
        int AlarmID = (int)id;
        AlarmDBHelper alarmDBHelper = new AlarmDBHelper(context);
        Alarm MyAlarm;
        if (AlarmID != 0) {
            MyAlarm = alarmDBHelper.getAlarm(AlarmID);
        } else {
            return;
        }

        NotificationManager nm = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);

        Notification.Builder NotificationBuilder = new Notification.Builder(context).
                setSmallIcon(R.drawable.ic_launcher).
                setContentTitle(MyAlarm.AlarmName).
                setContentText(context.getString(R.string.AlarmFor) + MyAlarm.Location).
                setDefaults(Notification.DEFAULT_ALL).
                setAutoCancel(true);

        Notification AlarmNotification;
        if (android.os.Build.VERSION.SDK_INT > 16){
            AlarmNotification = NotificationBuilder.build();
        } else{
            //noinspection deprecation
            AlarmNotification = NotificationBuilder.getNotification();
        }

        nm.notify((int)id, AlarmNotification);

        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        final MediaPlayer mediaPlayer = new MediaPlayer();
        final Context fContext = context;
        final long fAlarmID = AlarmID;
        if (powerManager.isScreenOn()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setIcon(R.drawable.ic_launcher);
            builder.setTitle(MyAlarm.getAlarmName());
            builder.setPositiveButton(R.string.stopAlarm, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    AlarmScreen.stopAlarm(fContext, mediaPlayer, fAlarmID);
                }
            });
            builder.setNegativeButton(R.string.Snooze, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    AlarmScreen.snoozeAlarm(fContext, mediaPlayer, fAlarmID);
                }
            });

            AlertDialog dialog = builder.create();
            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            dialog.show();
            AlarmScreen.startAlarm(context, mediaPlayer);
            return;
        }

        Intent AlarmScreen = new Intent(context, AlarmScreen.class);
        AlarmScreen.putExtra("AlarmID", AlarmID);
        AlarmScreen.putExtra("AlarmName", MyAlarm.getAlarmName());
        AlarmScreen.putExtra("AlarmLocation", MyAlarm.getLocation());
        AlarmScreen.putExtra("AlarmRecur", MyAlarm.getRecurType());

        java.text.DateFormat MyTimeFormat = DateFormat.getTimeFormat(context);
        java.text.DateFormat MyDateFormat = DateFormat.getDateFormat(context);

        MyTimeFormat.setTimeZone(TimeZone.getTimeZone(MyAlarm.getTimeZone()));
        MyDateFormat.setTimeZone(TimeZone.getTimeZone(MyAlarm.getTimeZone()));

        String AlarmLocalTime =
                        MyDateFormat.format(new Date(MyAlarm.getUTCTime())) + " " +
                        MyTimeFormat.format(new Date(MyAlarm.getUTCTime()));

        AlarmScreen.putExtra("AlarmLocalTime", AlarmLocalTime);

        AlarmScreen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(AlarmScreen);
    }
}

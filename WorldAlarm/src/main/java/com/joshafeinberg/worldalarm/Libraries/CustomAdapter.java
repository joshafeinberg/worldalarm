package com.joshafeinberg.worldalarm.Libraries;

import android.content.Context;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.joshafeinberg.worldalarm.AddAlarm;
import com.joshafeinberg.worldalarm.Database.Alarm;
import com.joshafeinberg.worldalarm.Database.AlarmDBHelper;
import com.joshafeinberg.worldalarm.R;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class CustomAdapter extends ArrayAdapter<Alarm> {

    private Context context;
    private int resource;
    private Callback callback;

    public interface Callback {
        void updateAdapter();
    }

    /**
     * {@link android.widget.ArrayAdapter}
     *
     * @param context   application context
     * @param resource  row layout resource
     * @param objects   all alarms
     */
    public CustomAdapter(Context context, int resource, List<Alarm> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
    }

    /**
     * {@inheritDoc}
     *
     * Additionally, adds all the row data, sets the active flag functionality, and makes the row
     * expandable
     *
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View RowView = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            RowView = inflater.inflate(this.resource, null);
        }

        Alarm alarm = getItem(position);

        addRowData(RowView, alarm);
        addActiveFlag(RowView, alarm);
        addExpandable(RowView);

        return RowView;
    }

    /**
     * Adds button to make alarm active/inactive
     * @param RowView expanded row view
     * @param alarm   alarm
     */
    private void addActiveFlag(View RowView, Alarm alarm) {
        final AlarmDBHelper alarmDBHelper = new AlarmDBHelper(context);
        final int alarmID = alarm.getAlarmID();
        boolean isChecked = alarm.getActive() == Alarm.ACTIVE;
        final long alarmUTC = alarm.getUTCTime();
        final Alarm fAlarm = alarm;

        ToggleButton AlarmActive = (ToggleButton) RowView.findViewById(R.id.alarmActive);
        AlarmActive.setChecked(isChecked);
        AlarmActive.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    alarmDBHelper.setAlarmActive(alarmID, true);
                    if (alarmUTC < new Date().getTime()) {

                        Calendar c = Calendar.getInstance();
                        Calendar c2 = Calendar.getInstance();
                        c2.setTimeInMillis(alarmUTC);
                        c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH),
                                c2.get(Calendar.HOUR_OF_DAY), c2.get(Calendar.MINUTE), c2.get(Calendar.SECOND));
                        if (c.getTimeInMillis() < new Date().getTime()) {
                            c.setTimeInMillis(c.getTimeInMillis() + 86400000L);
                        }
                        Alarm newAlarm = new Alarm(alarmID, fAlarm.getAlarmName(), c.getTimeInMillis(),
                                fAlarm.getTimeZone(), fAlarm.getLocation(), fAlarm.getRecurType(),
                                Alarm.ACTIVE);
                        alarmDBHelper.updateAlarm(newAlarm);
                        callback.updateAdapter();
                    } else {
                        AddAlarm.addAlarmToManager(context, alarmID, alarmUTC);
                    }
                } else {
                    alarmDBHelper.setAlarmActive(alarmID, false);
                    AddAlarm.removeAlarmFromManager(context, alarmID);
                }
            }
        });
    }


    /**
     * Makes button show/hide additional alarm information
     * @param RowView expanded row view
     */
    private void addExpandable(View RowView) {
        final Button AlarmDetailsShow = (Button) RowView.findViewById(R.id.alarmDetailsShow);
        final Button AlarmDetailsHide = (Button) RowView.findViewById(R.id.alarmDetailsHide);
        final TextView AlarmNextRun = (TextView) RowView.findViewById(R.id.alarmNextRun);
        final TextView AlarmNextRunLabel = (TextView) RowView.findViewById(R.id.alarmNextRunLabel);
        final TextView AlarmNextRunLocal = (TextView) RowView.findViewById(R.id.alarmNextRunLocal);
        final TextView AlarmNextRunLocalLabel = (TextView) RowView.findViewById(R.id.alarmNextRunLocalLabel);
        final TextView AlarmRecurType = (TextView) RowView.findViewById(R.id.alarmRecurType);
        final TextView AlarmRecurTypeLabel = (TextView) RowView.findViewById(R.id.alarmRecurTypeLabel);
        AlarmDetailsShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlarmDetailsShow.setVisibility(View.GONE);
                AlarmDetailsHide.setVisibility(View.VISIBLE);
                AlarmNextRunLabel.setVisibility(View.VISIBLE);
                AlarmNextRun.setVisibility(View.VISIBLE);
                AlarmNextRunLocalLabel.setVisibility(View.VISIBLE);
                AlarmNextRunLocal.setVisibility(View.VISIBLE);
                AlarmRecurTypeLabel.setVisibility(View.VISIBLE);
                AlarmRecurType.setVisibility(View.VISIBLE);
            }
        });

        AlarmDetailsHide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlarmDetailsShow.setVisibility(View.VISIBLE);
                AlarmDetailsHide.setVisibility(View.GONE);
                AlarmNextRunLabel.setVisibility(View.GONE);
                AlarmNextRun.setVisibility(View.GONE);
                AlarmNextRunLocalLabel.setVisibility(View.GONE);
                AlarmNextRunLocal.setVisibility(View.GONE);
                AlarmRecurTypeLabel.setVisibility(View.GONE);
                AlarmRecurType.setVisibility(View.GONE);
            }
        });
    }

    /**
     * Adds all row data to View
     * @param RowView  expanded row view
     * @param alarm    alarm
     */
    private void addRowData(View RowView, Alarm alarm) {

        // Alarm Name
        TextView AlarmName = (TextView) RowView.findViewById(R.id.alarmName);
        AlarmName.setText(alarm.getAlarmName());

        long UTCTimeMilli = alarm.getUTCTime();
        java.text.DateFormat MyTimeFormat = DateFormat.getTimeFormat(context);
        java.text.DateFormat MyDateFormat = DateFormat.getDateFormat(context);
        MyTimeFormat.setTimeZone(TimeZone.getTimeZone(alarm.getTimeZone()));
        MyDateFormat.setTimeZone(TimeZone.getTimeZone(alarm.getTimeZone()));
        String AlarmTimeZone = TimeZone.getTimeZone(alarm.getTimeZone()).getDisplayName(
                TimeZone.getTimeZone(alarm.getTimeZone()).inDaylightTime(new Date(UTCTimeMilli)),
                TimeZone.SHORT,
                Locale.getDefault());

        // Alarm Time
        TextView AlarmTime = (TextView) RowView.findViewById(R.id.alarmTime);
        AlarmTime.setText(MyTimeFormat.format(new Date(UTCTimeMilli)) + " " + AlarmTimeZone);

        // Alarm Next Run
        TextView AlarmNextRun = (TextView) RowView.findViewById(R.id.alarmNextRun);
        AlarmNextRun.setText(MyDateFormat.format(new Date(UTCTimeMilli)));

        MyTimeFormat.setTimeZone(TimeZone.getDefault());
        MyDateFormat.setTimeZone(TimeZone.getDefault());
        String LocalTimeZone = TimeZone.getDefault().getDisplayName(
                MyTimeFormat.getTimeZone().inDaylightTime(new Date(UTCTimeMilli)),
                TimeZone.SHORT,
                Locale.getDefault());

        // Alarm Next Run Local
        TextView AlarmNextRunLocal = (TextView) RowView.findViewById(R.id.alarmNextRunLocal);
        AlarmNextRunLocal.setText(MyDateFormat.format(new Date(UTCTimeMilli)) + " " +
                MyTimeFormat.format(new Date(UTCTimeMilli)) + " " + LocalTimeZone);

        // Alarm Recurring Type
        TextView AlarmRecurType = (TextView) RowView.findViewById(R.id.alarmRecurType);
        String[] RecurTypes = context.getResources().getStringArray(R.array.AlarmRecurringOptions);
        AlarmRecurType.setText(RecurTypes[alarm.getRecurType()]);

        // Alarm Location
        TextView AlarmLocation = (TextView) RowView.findViewById(R.id.alarmLocation);
        if (alarm.getLocation().equals(Alarm.LOCALALARM)) {
            AlarmLocation.setText("");
        } else {
            AlarmLocation.setText(alarm.getLocation());
        }
    }

    /**
     * sets callback for adapter
     * @param callback interface
     */
    public void setCallback(Callback callback) {
        this.callback = callback;
    }
}

package com.joshafeinberg.worldalarm;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.joshafeinberg.worldalarm.Database.Alarm;
import com.joshafeinberg.worldalarm.Database.AlarmDBHelper;
import com.joshafeinberg.worldalarm.Libraries.CustomAdapter;
import com.joshafeinberg.worldalarm.Libraries.SwipeDismissListViewTouchListener;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

public class AlarmList extends ListFragment implements CustomAdapter.Callback {

    private AlarmDBHelper alarmDBHelper;
    private List<Alarm> allAlarmsList;
    private CustomAdapter adapter;

    public AlarmList() {
    }

    /**
     * {@inheritDoc}
     *
     * Additionally, creates style links for rows in ListView
     * @param inflater {@link android.view.LayoutInflater}
     * @param container {@link android.view.ViewGroup}
     * @param savedInstanceState {@link android.os.Bundle}
     * @return a fragment containing the list of all alarms in the database
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        alarmDBHelper = new AlarmDBHelper(getActivity());

        allAlarmsList = alarmDBHelper.getAllAlarms();

        adapter = new CustomAdapter(getActivity(), R.layout.alarm_row, allAlarmsList);
        adapter.setCallback(this);
        setListAdapter(adapter);

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    /**
     * {@inheritDoc}
     *
     * Additionally, attaches the library {@link com.joshafeinberg.worldalarm.Libraries.SwipeDismissListViewTouchListener}
     * to each row to allow swipe for delete
     */
    @Override
    public void onStart() {
        super.onStart();

        ListView listView = getListView();
        SwipeDismissListViewTouchListener touchListener = new SwipeDismissListViewTouchListener(
            listView, new SwipeDismissListViewTouchListener.DismissCallbacks() {
                @Override
                public boolean canDismiss(int position) {
                    return true;
                }

                @Override
                public void onDismiss(ListView listView, int[] reverseSortedPositions) {
                    for (int position : reverseSortedPositions) {
                        Alarm item = (Alarm) adapter.getItem(position);
                        Integer alarmID = item.getAlarmID();
                        AddAlarm.removeAlarmFromManager(getActivity(), alarmID);
                        alarmDBHelper.deleteAlarm(alarmID);
                        allAlarmsList.remove(position);
                        adapter.remove(item);
                        adapter.notifyDataSetChanged();
                    }

                }
            });
        listView.setOnTouchListener(touchListener);
        listView.setOnScrollListener(touchListener.makeScrollListener());

    }

    /**
     * {@inheritDoc}
     *
     * Ensures that list always contains updated information
     */
    @Override
    public void onResume() {
        super.onResume();
        allAlarmsList = alarmDBHelper.getAllAlarms();
        adapter.clear();
        adapter.addAll(allAlarmsList);
        adapter.notifyDataSetChanged();
    }

    /**
     * {@inheritDoc}
     *
     * Additionally, creates a bundle to send data to {@link com.joshafeinberg.worldalarm.AddAlarm}
     */
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Alarm item = allAlarmsList.get(position);
        Integer alarmID = item.getAlarmID();
        Alarm MyAlarm = alarmDBHelper.getAlarm(alarmID);
        Bundle bundle = new Bundle();
        bundle.putCharSequence("AlarmName", MyAlarm.getAlarmName());
        bundle.putCharSequence("AlarmLocation", MyAlarm.getLocation());
        bundle.putString("AlarmTimeZone", MyAlarm.getTimeZone());
        Calendar c = new GregorianCalendar();
        c.setTimeInMillis(MyAlarm.getUTCTime());
        c.setTimeZone(TimeZone.getTimeZone(MyAlarm.getTimeZone()));
        bundle.putCharSequence("AlarmTimeHour", Integer.toString(c.get(Calendar.HOUR_OF_DAY)));
        bundle.putCharSequence("AlarmTimeMinute", Integer.toString(c.get(Calendar.MINUTE)));
        bundle.putCharSequence("AlarmDateMonth", Integer.toString(c.get(Calendar.MONTH)));
        bundle.putCharSequence("AlarmDateDay", Integer.toString(c.get(Calendar.DAY_OF_MONTH)));
        bundle.putCharSequence("AlarmDateYear", Integer.toString(c.get(Calendar.YEAR)));

        bundle.putInt("AlarmID", alarmID);
        bundle.putInt("AlarmRepeatMode", MyAlarm.getRecurType());
        bundle.putBoolean("AlarmUpdateMode", true);
        editAlarm(bundle);
    }

    /**
     * Pushes the {@link com.joshafeinberg.worldalarm.AddAlarm} fragment onto the screen
     * and loads up old alarm for editing
     * @param bundle contains old alarm data
     */
    public void editAlarm(Bundle bundle) {
        AddAlarm addAlarmFragment = new AddAlarm();
        addAlarmFragment.setArguments(bundle);

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.container, addAlarmFragment);
        transaction.addToBackStack(null);

        transaction.commit();
    }

    /**
     * updates adapter. called when item is marked as active
     */
    @Override
    public void updateAdapter() {
        allAlarmsList = alarmDBHelper.getAllAlarms();
        adapter.clear();
        adapter.addAll(allAlarmsList);
        adapter.notifyDataSetChanged();
    }
}
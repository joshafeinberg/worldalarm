package com.joshafeinberg.worldalarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.RemoteViews;

import com.joshafeinberg.worldalarm.Database.Alarm;
import com.joshafeinberg.worldalarm.Database.AlarmDBHelper;

import java.util.Calendar;
import java.util.Date;


public class AlarmWidget extends AppWidgetProvider {

    private static String INTENT = "com.joshafeinberg.worldalarm.AlarmWidget.WIDGET_UPDATE";

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

        RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                R.layout.appwidget);

        String amPM = "";
        Calendar c = Calendar.getInstance();
        int cHour = c.get(Calendar.HOUR_OF_DAY);
        if (!DateFormat.is24HourFormat(context)) {
            cHour = (cHour > 12) ? cHour - 12 : cHour;
            amPM = (c.get(Calendar.AM_PM) == Calendar.AM) ? "AM" : "PM";
        }
        cHour = (cHour == 0) ? 1 : cHour;
        int cMinute = c.get(Calendar.MINUTE);
        remoteViews.setTextViewText(R.id.alarmWidgetHours, String.format("%2d", cHour));
        remoteViews.setTextViewText(R.id.alarmWidgetMinutes, String.format("%02d", cMinute));
        remoteViews.setTextViewText(R.id.alarmWidgetAM_PM, amPM);

        // find alarm details
        AlarmDBHelper alarmDBHelper = new AlarmDBHelper(context);
        Alarm nextAlarm = alarmDBHelper.getNextAlarm();
        if (nextAlarm == null) {
            remoteViews.setViewVisibility(R.id.alarmWidgetNextRun, View.GONE);
        } else {
            long UTCTimeMilli = nextAlarm.getUTCTime();
            java.text.DateFormat MyTimeFormat = DateFormat.getTimeFormat(context);
            java.text.DateFormat MyDateFormat = DateFormat.getDateFormat(context);
            remoteViews.setViewVisibility(R.id.alarmWidgetNextRun, View.VISIBLE);
            //remoteViews.setTextViewText(R.id.alarmWidgetNextName, nextAlarm.getAlarmName());
            remoteViews.setTextViewText(R.id.alarmWidgetNextRunTime, MyTimeFormat.format(new Date(UTCTimeMilli)));
            remoteViews.setTextViewText(R.id.alarmWidgetNextRunDate, "(" + MyDateFormat.format(new Date(UTCTimeMilli)) + ")");
        }

        // make widget clickable
        Intent intent = new Intent(context, AlarmPage.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        remoteViews.setOnClickPendingIntent(R.id.alarmWidget, pendingIntent);

        for (int appWidgetId : appWidgetIds) {
            appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
        }

        // check if alarmmanager for clock update is set
        if (PendingIntent.getBroadcast(context, 0, new Intent(INTENT), PendingIntent.FLAG_NO_CREATE) == null) {
            onEnabled(context);
        }
    }

    private PendingIntent createClockTickIntent(Context context) {
        return PendingIntent.getBroadcast(context, 0, new Intent(INTENT), PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);

        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        // calculate exact time till next minute to shoot off next trigger
        int nextMinute = 60000 - (calendar.get(Calendar.SECOND) * 1000) - calendar.get(Calendar.MILLISECOND);
        alarmManager.setRepeating(AlarmManager.RTC, calendar.getTimeInMillis() + nextMinute, 60000, createClockTickIntent(context));
    }

    @Override
    public void onDisabled(Context context) {
        super.onDisabled(context);

        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(createClockTickIntent(context));
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);

        if (intent == null || intent.getAction() == null) {
            return;
        }

        if (intent.getAction().equals(INTENT)) {
            ComponentName thisAppWidget = new ComponentName(context.getPackageName(), getClass().getName());
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            if (appWidgetManager == null) {
                return;
            }
            int ids[] = appWidgetManager.getAppWidgetIds(thisAppWidget);
            onUpdate(context, appWidgetManager, ids);
        }
    }
}

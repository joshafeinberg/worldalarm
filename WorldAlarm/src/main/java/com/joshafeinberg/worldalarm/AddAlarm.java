package com.joshafeinberg.worldalarm;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.joshafeinberg.worldalarm.Database.Alarm;
import com.joshafeinberg.worldalarm.Database.AlarmDBHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

public class AddAlarm extends Fragment implements TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener, AdapterView.OnItemSelectedListener {


    private Button AlarmTimeButton, AlarmDateButton;
    private TextView AlarmTimeHour, AlarmTimeMinute;
    private TextView AlarmDateYear, AlarmDateMonth, AlarmDateDay;
    private boolean AlarmDateSet;
    private CheckBox AlarmLocationOn;
    private TextView AlarmLocalTime;
    private Spinner AlarmRecurring;
    private int AlarmRecurringSelected;
    private Button AlarmAddButton;
    private Button AlarmDeleteButton;
    private String AlarmTimeZone;
    private String FoundTimeZone;
    private long AlarmTimeUTCMilli;
    private RequestQueue queue;
    private boolean UpdateMode;
    private int UpdateAlarmID;

    public AddAlarm() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View addAlarmScreen = inflater.inflate(R.layout.fragment_alarm_add, container, false);

        if (addAlarmScreen != null) {
            AlarmTimeHour = (TextView) addAlarmScreen.findViewById(R.id.alarmTimeHour);
            AlarmTimeMinute = (TextView) addAlarmScreen.findViewById(R.id.alarmTimeMinute);
            AlarmDateYear = (TextView) addAlarmScreen.findViewById(R.id.alarmDateYear);
            AlarmDateMonth = (TextView) addAlarmScreen.findViewById(R.id.alarmDateMonth);
            AlarmDateDay = (TextView) addAlarmScreen.findViewById(R.id.alarmDateDay);
            AlarmLocalTime = (TextView) addAlarmScreen.findViewById(R.id.alarmLocalTime);
            AlarmRecurring = (Spinner) addAlarmScreen.findViewById(R.id.alarmRecurring);
            AlarmAddButton = (Button) addAlarmScreen.findViewById(R.id.addAlarmButton);
            AlarmDeleteButton = (Button) addAlarmScreen.findViewById(R.id.deleteAlarmButton);
            AlarmLocationOn = (CheckBox) addAlarmScreen.findViewById(R.id.setLocation);
            Calendar c = Calendar.getInstance();
            int cYear = c.get(Calendar.YEAR);
            int cMonth = c.get(Calendar.MONTH);
            int cDay = c.get(Calendar.DAY_OF_MONTH) + 1;
            AlarmDateYear.setText(Integer.toString(cYear));
            AlarmDateMonth.setText(Integer.toString(cMonth));
            AlarmDateDay.setText(Integer.toString(cDay));
            AlarmDateSet = false;

            addTimePicker(addAlarmScreen);
            addDatePicker(addAlarmScreen);
            getTimeZoneFromLocation(addAlarmScreen);
            setLocationCheckbox(addAlarmScreen);
            setAlarmRecurringItems(addAlarmScreen);
            setAddButton(addAlarmScreen);
            setDeleteButton(addAlarmScreen);
        }
        AlarmTimeZone = TimeZone.getDefault().getID();

        if (savedInstanceState == null) {
            savedInstanceState = this.getArguments();
        }

        if (savedInstanceState != null) {
            TextView AlarmName = (TextView) addAlarmScreen.findViewById(R.id.alarmName);
            AlarmName.setText(savedInstanceState.getCharSequence("AlarmName"));
            TextView AlarmLocation = (TextView) addAlarmScreen.findViewById(R.id.alarmLocation);
            AlarmLocation.setText(savedInstanceState.getCharSequence("AlarmLocation"));
            AlarmTimeZone = savedInstanceState.getString("AlarmTimeZone");
            if (AlarmLocation.getText().toString().equals(Alarm.LOCALALARM)) {
                AlarmLocation.setText("");
                AlarmTimeZone = TimeZone.getDefault().getID();
            }


            AlarmTimeHour.setText(savedInstanceState.getCharSequence("AlarmTimeHour"));
            AlarmTimeMinute.setText(savedInstanceState.getCharSequence("AlarmTimeMinute"));
            if (!AlarmTimeHour.getText().equals("") && !AlarmTimeMinute.getText().equals("")) {
                onTimeSet(null, Integer.valueOf(AlarmTimeHour.getText().toString()),
                        Integer.valueOf(AlarmTimeMinute.getText().toString()));
            }

            AlarmDateMonth.setText(savedInstanceState.getCharSequence("AlarmDateMonth"));
            AlarmDateDay.setText(savedInstanceState.getCharSequence("AlarmDateDay"));
            AlarmDateYear.setText(savedInstanceState.getCharSequence("AlarmDateYear"));
            if (!AlarmDateMonth.getText().equals("") && !AlarmDateDay.getText().equals("") &&
                    !AlarmDateYear.getText().equals("")) {
                onDateSet(null, Integer.valueOf(AlarmDateYear.getText().toString()),
                        Integer.valueOf(AlarmDateMonth.getText().toString()),
                        Integer.valueOf(AlarmDateDay.getText().toString()));
            }



            UpdateMode = savedInstanceState.getBoolean("AlarmUpdateMode", false);
            if (UpdateMode) {
                AlarmAddButton.setText(getResources().getText(R.string.editAlarm));
                UpdateAlarmID = savedInstanceState.getInt("AlarmID");
                AlarmRecurringSelected = savedInstanceState.getInt("AlarmRepeatMode");
                if (AlarmRecurringSelected != 0) {
                    CheckBox alarmRecurringOn = (CheckBox) addAlarmScreen.findViewById(R.id.setRepeat);
                    AlarmRecurring.setSelection(AlarmRecurringSelected);
                    alarmRecurringOn.setChecked(true);
                }
                if (!AlarmLocation.getText().toString().equals("")) {
                    AlarmLocationOn.setChecked(true);
                }
                AlarmDateSet = true;
                AlarmDeleteButton.setVisibility(View.VISIBLE);
            }

            getTimeStamp();
            handleTimeZone();
        }

        queue = Volley.newRequestQueue(getActivity());

        return addAlarmScreen;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        TextView AlarmName = (TextView) getActivity().findViewById(R.id.alarmName);
        outState.putCharSequence("AlarmName", AlarmName.getText());
        TextView AlarmLocation = (TextView) getActivity().findViewById(R.id.alarmLocation);
        outState.putCharSequence("AlarmLocation", AlarmLocation.getText());
        outState.putString("AlarmTimeZone", AlarmTimeZone);
        outState.putCharSequence("AlarmTimeHour", AlarmTimeHour.getText());
        outState.putCharSequence("AlarmTimeMinute", AlarmTimeMinute.getText());
        outState.putCharSequence("AlarmDateMonth", AlarmDateMonth.getText());
        outState.putCharSequence("AlarmDateDay", AlarmDateDay.getText());
        outState.putCharSequence("AlarmDateYear", AlarmDateYear.getText());
    }

    /**
     * Adds an action that generates the TimePickerDialog Fragment
     * when the Button <b>AlarmTimeButton</b> is hit
     *
     * @param  view fragment inflated view
     * @see         android.app.TimePickerDialog
     */
    public void addTimePicker(View view) {

        AlarmTimeButton = (Button) view.findViewById(R.id.alarmTime);
        final TextView AlarmLocation = (TextView) view.findViewById(R.id.alarmLocation);
        final AddAlarm ThisFragment = this;
        final TextView fAlarmTimeHour = AlarmTimeHour;
        final TextView fAlarmTimeMinute = AlarmTimeMinute;
        AlarmTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int hour;
                int minute;
                if ("".equals(fAlarmTimeHour.getText())) {
                    hour = c.get(Calendar.HOUR_OF_DAY);
                    minute = c.get(Calendar.MINUTE);
                } else {
                    hour = Integer.valueOf(fAlarmTimeHour.getText().toString());
                    minute = Integer.valueOf(fAlarmTimeMinute.getText().toString());
                }

                TimePickerDialog TimePicker = new TimePickerDialog(getActivity(), ThisFragment, hour, minute,
                        DateFormat.is24HourFormat(getActivity()));
                TimePicker.show();
                AlarmLocation.clearFocus();
            }
        });
    }

    /**
     * Adds an action that generates the DatePickerDialog Fragment
     * when the Button <b>AlarmDateButton</b> is hit
     *
     * @param  view fragment inflated view
     * @see         android.app.DatePickerDialog
     */
    public void addDatePicker(View view) {
        AlarmDateButton = (Button) view.findViewById(R.id.alarmDate);
        final TextView AlarmLocation = (TextView) view.findViewById(R.id.alarmLocation);
        final AddAlarm ThisFragment = this;
        final TextView fAlarmDateYear = AlarmDateYear;
        final TextView fAlarmDateMonth = AlarmDateMonth;
        final TextView fAlarmDateDay = AlarmDateDay;
        AlarmDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int year, month, day;
                if (!AlarmDateSet) {
                    final Calendar c = Calendar.getInstance();
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    if (!AlarmTimeHour.getText().toString().equals("") &&
                            !AlarmTimeMinute.getText().toString().equals("")) {
                        int hour = Integer.valueOf(AlarmTimeHour.getText().toString());
                        int minute= Integer.valueOf(AlarmTimeMinute.getText().toString());
                        if (hour > c.get(Calendar.HOUR_OF_DAY) ||
                                (hour == c.get(Calendar.HOUR_OF_DAY) && minute > c.get(Calendar.MINUTE))) {
                            day = c.get(Calendar.DAY_OF_MONTH);
                        } else {
                            day = c.get(Calendar.DAY_OF_MONTH) + 1;
                        }
                    } else {
                        day = c.get(Calendar.DAY_OF_MONTH);
                    }
                } else {
                    year = Integer.valueOf(fAlarmDateYear.getText().toString());
                    month = Integer.valueOf(fAlarmDateMonth.getText().toString());
                    day = Integer.valueOf(fAlarmDateDay.getText().toString());
                }

                DatePickerDialog DatePicker = new DatePickerDialog(getActivity(), ThisFragment, year, month, day);
                DatePicker.getDatePicker().setMinDate(new Date().getTime() - 1000);
                DatePicker.show();
                AlarmLocation.clearFocus();
            }
        });
    }

    /**
     * Adds a listener to the TextView <b>AlarmLocation</b> that
     * onFocusChange will look up a locations latitude and longitude
     * using googleapis. Once data is received, it is sent to
     * {@link #handleLocation(String, String)}
     *
     * @param  view fragment inflated view
     * @see    #handleLocation(String, String)
     */
    public void getTimeZoneFromLocation(View view) {
        TextView alarmLocation = (TextView) view.findViewById(R.id.alarmLocation);
        alarmLocation.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                final TextView fAlarmLocation = (TextView) v;
                String AlarmLocationStr = fAlarmLocation.getText().toString();
                if (AlarmLocationStr.equals("")) {
                    return;
                }
                AlarmLocationStr = AlarmLocationStr.replace(" ", "%20");
                String url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + AlarmLocationStr + "&sensor=false";

                JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        String lat = "", lng = "";
                        try {
                            JSONArray results = response.getJSONArray("results");
                            JSONObject geometry = results.getJSONObject(0).getJSONObject("geometry");
                            JSONObject location = geometry.getJSONObject("location");
                            lat = location.getString("lat");
                            lng = location.getString("lng");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (!lat.equals("") && !lng.equals("")) {
                            handleLocation(lat, lng);
                        } else if (!fAlarmLocation.getText().toString().equals("")) {
                            Toast toast = Toast.makeText(getActivity(),
                                    getString(R.string.LocationNotFound),
                                    Toast.LENGTH_SHORT);
                            toast.show();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast toast = Toast.makeText(getActivity(),
                                getString(R.string.ErrorFindingLocation),
                                Toast.LENGTH_SHORT);
                        toast.show();

                    }
                }
                );
                queue.add(jsObjRequest);
            }
        });
    }

    /**
     * Given a latitude and longitude, uses googleapis to return a timezone
     * for the user entered location
     *
     * @param  lat string containing latitude
     * @param  lng string containing longitude
     */
    public void handleLocation(String lat, String lng) {
        long TimeStamp = getTimeStamp();

        String url = "https://maps.googleapis.com/maps/api/timezone/json?location="
                +lat+","+lng+"&timestamp="+TimeStamp + "&sensor=false";

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    AlarmTimeZone = response.getString("timeZoneId");
                    handleTimeZone();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast toast = Toast.makeText(getActivity(),
                        getString(R.string.ErrorFindingTimeZone),
                        Toast.LENGTH_SHORT);
                toast.show();

            }
        });
        queue.add(jsObjRequest);


    }

    /**
     * Checks to see if data has been entered into the date and time fields.
     *
     * @return  if fields are filled and timezone set
     */
    public boolean isDataFilled() {
        String yearStr = AlarmDateYear.getText().toString();
        String monthStr = AlarmDateMonth.getText().toString();
        String dayStr = AlarmDateDay.getText().toString();
        String hourStr = AlarmTimeHour.getText().toString();
        String minuteStr = AlarmTimeMinute.getText().toString();

        return (!yearStr.equals("") && !monthStr.equals("") && !dayStr.equals("") &&
                !hourStr.equals("") && !minuteStr.equals("") && !AlarmTimeZone.equals(""));
    }

    /**
     * Fixes all data on screen once a timezone is received or if the
     * time or date have changed
     */
    public void handleTimeZone() {

        if (isDataFilled()) {
            getTimeStamp();
            java.text.DateFormat MyDateFormat = DateFormat.getDateFormat(getActivity());
            java.text.DateFormat MyTimeFormat = DateFormat.getTimeFormat(getActivity());
            MyDateFormat.setTimeZone(TimeZone.getTimeZone(AlarmTimeZone));
            MyTimeFormat.setTimeZone(TimeZone.getTimeZone(AlarmTimeZone));
            TimeZone MyTimeZone = TimeZone.getTimeZone(AlarmTimeZone);
            String TimeZoneShort =
                    MyTimeZone.getDisplayName(MyTimeZone.inDaylightTime(new Date(AlarmTimeUTCMilli)),
                            TimeZone.SHORT,
                            Locale.getDefault());

            AlarmTimeButton.setText(MyTimeFormat.format(new Date(AlarmTimeUTCMilli)) + " "
                    + TimeZoneShort);

            MyDateFormat.setTimeZone(TimeZone.getDefault());
            MyTimeFormat.setTimeZone(TimeZone.getDefault());

            String LocalTimeZone = MyTimeFormat.getTimeZone().getDisplayName(
                    MyTimeFormat.getTimeZone().inDaylightTime(new Date(AlarmTimeUTCMilli)),
                    TimeZone.SHORT,
                    Locale.getDefault());

            AlarmLocalTime.setText(
                    getString(R.string.LocalTime) +
                    MyDateFormat.format(new Date(AlarmTimeUTCMilli)) + " " +
                    MyTimeFormat.format(new Date(AlarmTimeUTCMilli)) + " " + LocalTimeZone);

        }

    }

    /**
     * Calculates the timestamp in milliseconds and microseconds
     * using all data entered. Defaults to current date/time
     *
     * @return  time in microseconds
     */
    public long getTimeStamp() {
        Calendar c = Calendar.getInstance();
        int cYear = c.get(Calendar.YEAR);
        int cMonth = c.get(Calendar.MONTH);
        int cDay = c.get(Calendar.DAY_OF_MONTH);
        int cHour = c.get(Calendar.HOUR_OF_DAY);
        int cMinute = c.get(Calendar.MINUTE);

        String yearStr = AlarmDateYear.getText().toString();
        int year = (yearStr.equals("")) ? cYear : Integer.valueOf(yearStr);
        String monthStr = AlarmDateMonth.getText().toString();
        int month = (monthStr.equals("")) ? cMonth : Integer.valueOf(monthStr);
        String dayStr = AlarmDateDay.getText().toString();
        int day = (dayStr.equals("")) ? cDay : Integer.valueOf(dayStr);

        String hourStr = AlarmTimeHour.getText().toString();
        int hour = (hourStr.equals("")) ? cHour : Integer.valueOf(hourStr);
        String minuteStr = AlarmTimeMinute.getText().toString();
        int minute = (minuteStr.equals("")) ? cMinute : Integer.valueOf(minuteStr);

        Calendar MyCal = new GregorianCalendar(year, month, day, hour, minute);
        MyCal.setTimeZone(TimeZone.getTimeZone(AlarmTimeZone));
        AlarmTimeUTCMilli = MyCal.getTimeInMillis();

        return AlarmTimeUTCMilli / 1000;
    }

    /**
     * Sets checkbox to display location options
     * @param view fragment inflated view
     */
    private void setLocationCheckbox(View view) {
        final TextView AlarmLocation = (TextView) view.findViewById(R.id.alarmLocation);
        final TextView AlarmLocalTime = (TextView) view.findViewById(R.id.alarmLocalTime);
        AlarmLocationOn.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    AlarmLocation.setVisibility(View.VISIBLE);
                    AlarmLocalTime.setVisibility(View.VISIBLE);
                    if (FoundTimeZone != null) {
                        AlarmTimeZone = FoundTimeZone;
                        handleTimeZone();
                    }
                } else {
                    AlarmLocation.setVisibility(View.GONE);
                    AlarmLocalTime.setVisibility(View.GONE);
                    FoundTimeZone = AlarmTimeZone;
                    AlarmTimeZone = TimeZone.getDefault().getID();
                    handleTimeZone();
                }
            }
        });
    }


    /**
     * {@inheritDoc}
     *
     * Additionally, resolves new timestamp with fixes for the timezone entered
     */
    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

        AlarmTimeHour.setText(Integer.toString(hourOfDay));
        AlarmTimeMinute.setText(Integer.toString(minute));
        getTimeStamp();

        java.text.DateFormat MyTimeFormat = DateFormat.getTimeFormat(getActivity());
        MyTimeFormat.setTimeZone(TimeZone.getTimeZone(AlarmTimeZone));

        AlarmTimeButton.setText(MyTimeFormat.format(new Date(AlarmTimeUTCMilli)));

        if (view != null) {
            handleTimeZone();
        }
    }

    /**
     * {@inheritDoc}
     *
     * Additionally, resolves new timestamp with fixes for
     * the timezone entered
     */
    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {

        AlarmDateYear.setText(Integer.toString(year));
        AlarmDateMonth.setText(Integer.toString(month));
        AlarmDateDay.setText(Integer.toString(day));
        getTimeStamp();

        java.text.DateFormat MyDateFormat = DateFormat.getDateFormat(getActivity());
        MyDateFormat.setTimeZone(TimeZone.getTimeZone(AlarmTimeZone));

        AlarmDateButton.setText(MyDateFormat.format(new Date(AlarmTimeUTCMilli)));

        if (view != null) {
            handleTimeZone();
        }

        AlarmDateSet = true;
        Log.i("worldalarm", "onDateSet => " + AlarmTimeUTCMilli);
    }

    /**
     * Sets the AlarmRecurring spinner and the checkbox to hide and display the item
     * @param view fragment inflated view
     */
    public void setAlarmRecurringItems(View view) {
        CheckBox alarmRecurringOn = (CheckBox) view.findViewById(R.id.setRepeat);
        alarmRecurringOn.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    AlarmRecurring.setVisibility(View.VISIBLE);
                } else {
                    AlarmRecurring.setVisibility(View.GONE);
                }
            }
        });

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.AlarmRecurringOptions, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        AlarmRecurring.setAdapter(adapter);
        AlarmRecurring.setOnItemSelectedListener(this);
        AlarmRecurring.setSelection(AlarmRecurringSelected);
    }

    /**
     * {@inheritDoc}
     *
     * Additionally, sets the alarm recurring option
     */
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        AlarmRecurringSelected = position;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    /**
     * Adds an onClickListener to <b>AlarmAddButton</b> that confirms
     * alarm is in the future and stores in the database
     *
     * @param  view fragment inflated view
     * @see         com.joshafeinberg.worldalarm.Database.AlarmDBHelper
     * @see         com.joshafeinberg.worldalarm.Database.Alarm
     */
    private void setAddButton(View view) {

        final View fView = view;

        AlarmAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isDataFilled()) {

                    if (AlarmTimeUTCMilli < new Date().getTime()) {
                        Toast.makeText(getActivity().getBaseContext(),
                                getString(R.string.AlarmPastError),
                                Toast.LENGTH_SHORT).show();
                        return;
                    }

                    TextView AlarmName = (TextView) fView.findViewById(R.id.alarmName);
                    TextView AlarmLocation = (TextView) fView.findViewById(R.id.alarmLocation);

                    String NewAlarmTimeZone;
                    String NewAlarmLocation;
                    if (AlarmLocationOn.isChecked()) {
                        NewAlarmTimeZone = AlarmTimeZone;
                        NewAlarmLocation = AlarmLocation.getText().toString();
                    } else {
                        NewAlarmTimeZone = TimeZone.getDefault().getID();
                        NewAlarmLocation = Alarm.LOCALALARM;
                    }
                    AlarmDBHelper alarmDBHelper = new AlarmDBHelper(getActivity());
                    Alarm NewAlarm = new Alarm(AlarmName.getText().toString(),
                            AlarmTimeUTCMilli,
                            NewAlarmTimeZone,
                            NewAlarmLocation,
                            AlarmRecurringSelected,
                            Alarm.ACTIVE);
                    long NewAlarmId;
                    int addString;
                    if (UpdateMode) {
                        NewAlarm.AlarmID = UpdateAlarmID;
                        removeAlarmFromManager(getActivity(), NewAlarm.AlarmID);
                        alarmDBHelper.updateAlarm(NewAlarm);
                        NewAlarmId = UpdateAlarmID;
                        addString = R.string.AlarmEdited;
                    } else {
                        NewAlarmId = alarmDBHelper.addAlarm(NewAlarm);
                        addString = R.string.AlarmAdded;
                    }
                    Log.i("worldalarm", "New Alarm ID => " + NewAlarmId);
                    addAlarmToManager(getActivity(), NewAlarmId, AlarmTimeUTCMilli);

                    Toast.makeText(getActivity().getBaseContext(),
                            getString(R.string.Alarm) + " " + AlarmName.getText() + " " + getString(addString),
                            Toast.LENGTH_LONG).show();

                    getFragmentManager().popBackStack();

                }
            }
        });

    }

    private void setDeleteButton(View view) {

        AlarmDeleteButton.setVisibility(View.GONE);

        AlarmDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlarmDBHelper alarmDBHelper = new AlarmDBHelper(getActivity());
                Integer alarmID = UpdateAlarmID;
                AddAlarm.removeAlarmFromManager(getActivity(), alarmID);
                alarmDBHelper.deleteAlarm(alarmID);
                getFragmentManager().popBackStack();
                /*allAlarmsList.remove(position);
                adapter.remove(item);
                adapter.notifyDataSetChanged();*/
            }
        });

    }

    /**
     * Adds an alarm to the {@link android.app.AlarmManager}
     * @param context    activity context
     * @param AlarmID    alarm database id
     * @param AlarmTime  alarm utc time
     */
    public static void addAlarmToManager(Context context, long AlarmID, long AlarmTime) {
        Intent myIntent = new Intent(context, AlarmReceiver.class);
        myIntent.putExtra("id", AlarmID);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, (int)AlarmID, myIntent,0);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC, AlarmTime, pendingIntent);

        // update widget
        Intent intent = new Intent(context,AlarmWidget.class);
        intent.setAction("android.appwidget.action.APPWIDGET_UPDATE");
        //noinspection ConstantConditions
        int[] ids = AppWidgetManager.getInstance(context).getAppWidgetIds(new ComponentName(context, AlarmWidget.class));
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS,ids);
        context.sendBroadcast(intent);
    }

    /**
     * Removes an alarm from the {@link android.app.AlarmManager}
     * @param context  activity context
     * @param AlarmID  alarm database id
     */
    public static void removeAlarmFromManager(Context context, long AlarmID) {
        Intent myIntent = new Intent(context, AlarmReceiver.class);
        myIntent.putExtra("id", AlarmID);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, (int)AlarmID, myIntent,0);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
    }

    /**
     * Updates a recurring alarm and deactivates non-recurring alarms
     * @param context activity context
     * @param alarmID alarm database id
     */
    public static void updateRecurringAlarm(Context context, int alarmID) {
        AlarmDBHelper alarmDBHelper = new AlarmDBHelper(context);
        Alarm MyAlarm = alarmDBHelper.getAlarm(alarmID);
        Calendar cal1 = new GregorianCalendar();
        Calendar cal2 = new GregorianCalendar();

        cal1.setTimeInMillis(MyAlarm.getUTCTime());
        cal2.setTimeInMillis(MyAlarm.getUTCTime());

        long AddTime = 0;
        switch (MyAlarm.getRecurType()) {
            case 0:
                //alarmDBHelper.deleteAlarm(alarmID);
                alarmDBHelper.setAlarmActive(alarmID, false);
                return;
            case 1:
                AddTime = 86400000L;
                break;
            case 2:
                boolean isWeekday = false;
                do {
                    cal2.add(Calendar.DAY_OF_MONTH, 1);
                    int day = cal2.get(Calendar.DAY_OF_WEEK);
                    if (day != 1 && day != 7) {
                        isWeekday = true;
                    }
                } while (!isWeekday);
                AddTime = (cal2.getTime().getTime() - cal1.getTime().getTime());
                break;
            case 3:
                boolean isWeekend = false;
                do {
                    cal2.add(Calendar.DAY_OF_MONTH, 1);
                    int day = cal2.get(Calendar.DAY_OF_WEEK);
                    if (day == 1 || day == 7) {
                        isWeekend = true;
                    }
                } while (!isWeekend);
                AddTime = (cal2.getTime().getTime() - cal1.getTime().getTime());
                break;
            case 4:
                AddTime = (86400000 * 7);
                break;
            case 5:
                cal2.add(Calendar.MONTH, 1);
                AddTime = (cal2.getTime().getTime() - cal1.getTime().getTime());
                break;
            case 6:
                cal2.add(Calendar.YEAR, 1);
                AddTime = (cal2.getTime().getTime() - cal1.getTime().getTime());
                break;
        }

        alarmDBHelper.setAlarmActive(alarmID, true);
        addAlarmToManager(context, alarmID, MyAlarm.getUTCTime() + AddTime);
        MyAlarm.UTCTime = MyAlarm.getUTCTime() + AddTime;
        alarmDBHelper.updateAlarm(MyAlarm);
    }
}